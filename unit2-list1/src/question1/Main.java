package question1;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BinarySearchTree tree1 = new BinarySearchTree();
		BinarySearchTree tree2 = new BinarySearchTree();
		
		
		tree1.insert(5);
		tree1.insert(2);
		tree1.insert(3);
		tree1.insert(10);
		tree1.insert(8);
		
		tree2.insert(8);
		tree2.insert(5);
		tree2.insert(6);
		tree2.insert(1);
		tree2.insert(10);
		
		
		boolean isComplete = tree1.isComplete();
		boolean isComplete2 = tree2.isComplete();
		
		if(isComplete) System.out.print("BST 1 - Complete\n");
		else System.out.print("BST 1 - Not Complete\n");

		if(isComplete2) System.out.print("BST 2 - Complete\n");
		else System.out.print("BST 2 - Not Complete\n");
	}

}
