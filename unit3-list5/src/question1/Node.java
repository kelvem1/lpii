package question1;

public class Node<T> {

	public T value;
	public int height;

	public Node <T> left;
	public Node <T> right;

	public Node(T value) {
		this.value = value;
		this.height = 1;
	}

	public boolean isLeaf() {
		return left == null && right == null;
	}

	public boolean hasLeft() {
		return left != null;
	}

	public boolean hasRight() {
		return right != null;
	}

	public void setChilds(Node <T> left, Node <T> right) {
		this.left = left;
		this.right = right;
	}
/*
	public int hashCode() {
		final int prime = 31;
		T result = 1;
		result = prime * result + value;
		return result;
	}

	public String toString() {
		return Integer.toString(value);
	}
	*/
/*
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tree other = (Tree) obj;
		if (value != other.value)
			return false;
		return true;
	}
	*/
}
