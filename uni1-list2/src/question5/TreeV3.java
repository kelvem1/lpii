package question5;

public class TreeV3 extends Tree {
	public boolean insert(int value) {
		if (root == null) {
			root = new Node(value);
			return true;
		} else {
			return insert(root, value);
		}
	}

	private boolean insert(Node node, int value) {
		if (value > node.value) {
			if (node.hasRight()) {
				return insert(node.right, value);
			} else {
				node.right = new Node(value);
			}
		} else if (value < node.value) {
			if (node.hasLeft()) {
				return insert(node.left, value);
			} else {
				node.left = new Node(value);
			}
		} else {
			return false; // contains value
		}
		return true;
	}
    
	public boolean remove(int value) {
		remove(root, value);
		return true;
	}

	private boolean remove(Node root, int value)  { 
		if (root == null) {
			return false;
		} else if (root.left == null && root.right == null) {
			root = null;
			return true;
		} else if (root.left == null && root.right != null) {
			if (root.right.value == value) {
				root = root.right;
			}
			else {
				remove(root.right, value);
			}
		} else if (root.left != null && root.right == null) {
			if (root.left.value == value) {
				root = root.left;
			} else {
				remove(root.left, value);
			}
		} else {
			if (value > root.value) {
				int minRight = minValue(root.right);
				remove(root.right, minRight);
				root.setValue(minRight);
			}
			else {
				int minRight = minValue(root.left);
				remove(root.right, minRight);
				root.setValue(minRight);
			}
		}
		return true;
    } 
	
	private int minValue(Node root) {
		if (root.left == null) {
			return root.getValue();
		} else {

			return minValue(root.left);
		}
	}

 
}
