package question3;

public class TreeV2 extends Tree{
	
	public boolean insert(int value) {
		root = insert(root, value);
		return true;
	}
	
	
	private Node insert(Node node, int value) {
		if(node == null)
			return new Node(value);
		if (value < node.value) 
			 node.left = insert(node.left, value);
		 else if (value > node.value)
			 node.right= insert(node.right, value);
		return node;
	}
	
	public boolean equals(Node tree1) {
		return equals(root, tree1);
	}
	
	
	public boolean equals(Node tree, Node tree1) {
		
		if(tree1 == null && tree == null) {
			return true;
		}
		if(tree1 == null || tree == null) return false;
		if (tree.value != tree1.value) return false;
		if (equals(tree.left, tree1.left) == false) return false;
		if (equals(tree.right, tree1.right) == false) return false;
		return true;
	}
	
}
