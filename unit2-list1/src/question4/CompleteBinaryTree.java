package question4;

public class CompleteBinaryTree extends BinarySearchTree {

/////////////////////////////////////////////////////// insert()
	public boolean insert(int value) {
		if (root == null) {
			root = new Node(value);
			return true;
		} else {
			return insert(root, 0, value);
		}
	}

	private boolean insert(Node node, int index, int value) {
		boolean flag = false;
		while (!flag) {
			Node current = node;
			for (int i = 1; i < index; i++) {
				current = current.left;
			}
			if (!current.hasLeft()) {
				current.left = new Node(value);
				flag = true;
			} else if (!current.hasRight()) {
				current.right = new Node(value);
				flag = true;
			} else {
				current = node;
				for (int i = 1; i < index; i++) {
					current = current.right;
				}
				if (!current.hasLeft()) {
					current.left = new Node(value);
					flag = true;
				} else if (!current.hasRight()) {
					current.right = new Node(value);
					flag = true;
				}
			}
			index++;
		}
		if (flag) {
			return true;
		}

		return true;
	}

///////////////////////////////////////////////////////
/////////////////////////////////////////////////////// maxNivel()
	public int maxNivel() {
		if (root == null) {
			return 0;
		}
		return maxNivel(root, 0, 1);
	}

	private int maxNivel(Node root, int maxNiv, int level) {
		int maxNivLeft, maxNivRigth;
		if (root != null) {
			if (level > maxNiv) {
				maxNiv = level;
			}

			maxNivRigth = maxNivel(root.right, maxNiv, level + 1);
			maxNivLeft = maxNivel(root.left, maxNiv, level + 1);

			if (maxNivLeft >= maxNivRigth && maxNivLeft > maxNiv) {
				maxNiv = maxNivLeft;
			}
			if (maxNivRigth >= maxNivLeft && maxNivRigth > maxNiv) {
				maxNiv = maxNivRigth;
			}
		}
		return maxNiv;
	}

///////////////////////////////////////////////////////
/////////////////////////////////////////////////////// getNodeLast()
	public Node getNodeLast() {
		if (root == null) {
			return null;
		}
		return getNodeLast(root, maxNivel(), 1);
	}

	private Node getNodeLast(Node root, int maxNiv, int level) {

		if (root != null) {
			if (maxNiv == level + 1 && !root.isLeaf()) {
				if (root.hasRight()) {
					return root.right;
				}
				return root.left;

			} else {
				Node nodeLast1 = getNodeLast(root.right, maxNiv, level + 1);
				Node nodeLast2 = getNodeLast(root.left, maxNiv, level + 1);

				if (nodeLast1 != null) {
					return nodeLast1;
				} else if (nodeLast2 != null) {
					return nodeLast2;
				}
			}
		}
		return null;
	}

///////////////////////////////////////////////////////
/////////////////////////////////////////////////////// removeNodeLast()
	public Node removeNodeLast() {
		if (root == null) {
			return null;
		}
		return removeNodeLast(root, getNodeLast(), maxNivel(), 1);
	}

	private Node removeNodeLast(Node root, Node removeNode, int maxNiv, int level) {

		if (root != null) {
			if (maxNiv == level + 1 && !root.isLeaf()) {
				if (root.right == removeNode) {
					Node node = root.right;
					root.right = null;
					return node;
				} else if (root.left == removeNode) {
					Node node = root.left;
					root.left = null;
					return node;
				}

			} else {
				Node nodeLast1 = removeNodeLast(root.right, removeNode, maxNiv, level + 1);
				Node nodeLast2 = removeNodeLast(root.left, removeNode, maxNiv, level + 1);

				if (nodeLast1 != null) {
					return nodeLast1;
				} else if (nodeLast2 != null) {
					return nodeLast2;
				}
			}
		}
		return null;
	}

///////////////////////////////////////////////////////
/////////////////////////////////////////////////////// remove()
	public boolean remove(int value) {
		return remove(root, null, value);
	}

	private boolean remove(Node node, Node parent, int value) {
		boolean removed1 = false;
		boolean removed2 = false;
		if (node == null) {
			return false;
		} else if (node.value == value) {
			if (node.isLeaf() && parent == null) {
				root = null;
				return true;
			}

			Node nodeLast = removeNodeLast();

			nodeLast.right = node.right;
			nodeLast.left = node.left;
			if (parent == null) {
				root = nodeLast;
				return true;
			}
			if (parent.right == node) {
				parent.right = nodeLast;
				return true;
			} else if (parent.left == node) {
				parent.left = nodeLast;
				return true;
			}
			return true;
		}
		removed1 = remove(node.right, node, value);
		removed2 = remove(node.left, node, value);

		if (removed1 || removed2) {
			return true;
		} else {
			return false;
		}
	}

///////////////////////////////////////////////////////
/////////////////////////////////////////////////////// inOrder()
	public void inOrder() {
		inOrder("", root, 1, false);
	}

	private void inOrder(String prefix, Node n, Integer level, boolean isLeft) {
		if (n != null) {

			inOrder(prefix + "    ", n.right, level + 1, false);
			System.out.println(prefix + n.value + " [Level:" + level + "]");
			inOrder(prefix + "    ", n.left, level + 1, true);

		}
	}

///////////////////////////////////////////////////////

}
