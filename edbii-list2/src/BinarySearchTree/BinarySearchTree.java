package BinarySearchTree;

import java.util.LinkedList;

public class BinarySearchTree {

	protected Node root;

	public int height() {
		return height(root, 0);
	}

	private int height(Node node, int level) {
		if (node == null) { // root
			return level;
		}
		return Math.max(height(node.left, level + 1), height(node.right, level + 1));
	}

	public boolean isComplete() {
		return isComplete(root, 1, this.height());
	}

	public boolean isComplete(Node node, int currentLevel, int lastLevel) {
		if (node == null) {
			return currentLevel == lastLevel || currentLevel == lastLevel + 1;
		}
		return isComplete(node.left, currentLevel + 1, lastLevel)
				&& isComplete(node.right, currentLevel + 1, lastLevel);
	}

	public boolean isFull() {
		return isFull(root);
	}

	private boolean isFull(Node root) {
		if (root == null)
			return true;

		if (root.left == null && root.right == null)
			return true;

		if ((root.left != null) && (root.right != null))
			return (isFull(root.left) && isFull(root.right));

		return false;
	}

	public boolean search(int value) {
		return search(root, value);
	}

	private boolean search(Node root, int value) {
		if (root == null) {
			return false;
		}
		if (root.value == value)
			return true;

		// Key is greater than root's key
		if (root.value < value)
			return search(root.right, value);

		if (root.value > value)
			return search(root.left, value);

		return false;
	}

	public boolean insert(int value) {
		if (root == null) {
			root = new Node(value);
			return true;
		} else {
			return insert(root, value);
		}
	}

	private boolean insert(Node node, int value) {
		if (value > node.value) {
			if (node.hasRight()) {
				return insert(node.right, value);
			} else {
				node.right = new Node(value);
			}
		} else if (value < node.value) {
			if (node.hasLeft()) {
				return insert(node.left, value);
			} else {
				node.left = new Node(value);
			}
		} else {
			return false; // contains value
		}
		return true;
	}

	public boolean contains(int value) {
		return contains(root, value);
	}

	private boolean contains(Node node, int value) {
		if (node == null) {
			return false;
		} else {
			if (node.value == value) {
				return true;
			} else if (value > node.value) {
				return contains(node.right, value);
			} else if (value < node.value) {
				return contains(node.left, value);
			}
		}
		return false;
	}

	public boolean remove(int value) {
		return remove(root, null, value);
	}

	private boolean remove(Node node, Node parent, int value) {
		if (node == null) {
			return false;
		} else if (node.value == value) {
			if (node.isLeaf()) {
				updateChild(node, parent, null);
			} else if (node.hasLeft() && !node.hasRight()) {
				updateChild(node, parent, node.left);
			} else if (!node.hasLeft() && node.hasRight()) {
				updateChild(node, parent, node.right);
			} else {
				Node child = node.right;
				if (!child.hasLeft()) {
					child.left = node.left;
					updateChild(node, parent, child);
				} else {
					Node successor = removeSuccessor(child);
					successor.left = node.left;
					successor.right = node.right;
					updateChild(node, parent, successor);
				}
			}
		} else if (value > node.value) {
			return remove(node.right, node, value);
		} else if (value < node.value) {
			return remove(node.left, node, value);
		}
		return true;
	}

	private void updateChild(Node node, Node parent, Node child) {
		if (parent == null) {
			root = child;
		} else if (node.value > parent.value) {
			parent.right = child;
		} else if (node.value < parent.value) {
			parent.left = child;
		}
	}

	protected Node removeSuccessor(Node node) {
		if (!node.left.hasLeft()) {
			Node successor = node.left;
			node.left = successor.right;
			return successor;
		} else {
			return removeSuccessor(node.left);
		}
	}

	public void levelOrder() {
		LinkedList<Node> queue = new LinkedList<Node>();
		queue.addLast(root);
		while (!queue.isEmpty()) {
			Node current = queue.removeFirst();
			if (current != null) {
				System.out.println(root);
				queue.addLast(current.left);
				queue.addLast(current.right);
			}
		}
	}

	public int counNodes(Node root) {
		Node current, pre;
		int count = 0;

		if (root == null)
			return count;

		current = root;
		while (current != null) {
			if (current.left == null) {
				count++;
				current = current.right;
			} else {
				pre = current.left;

				while (pre.right != null && pre.right != current)
					pre = pre.right;

				if (pre.right == null) {
					pre.right = current;
					current = current.left;
				} else {
					pre.right = null;

					count++;
					current = current.right;
				}
			}
		}

		return count;
	}
	
	public int findMedian() {
		return findMedian(root); 
	}
	

	private int findMedian(Node root) {
		if (root == null)
			return 0;

		int count = counNodes(root);
		int currCount = 0;
		Node current = root, pre = null, prev = null;

		while (current != null) {
			if (current.left == null) {
				currCount++;

			
				if (count % 2 != 0 && currCount == (count + 1) / 2)
					return prev.value;

				else if (count % 2 == 0 && currCount == (count / 2) + 1)
					return (prev.value + current.value) / 2;

				
				prev = current;

			
				current = current.right;
			} else {
			
				pre = current.left;
				while (pre.right != null && pre.right != current)
					pre = pre.right;

		
				if (pre.right == null) {
					pre.right = current;
					current = current.left;
				}

				else {
					pre.right = null;
					prev = pre;
					currCount++;

					if (count % 2 != 0 && currCount == (count + 1) / 2)
						return current.value;

					else if (count % 2 == 0 && currCount == (count / 2) + 1)
						return (prev.value + current.value) / 2;

					prev = current;
					current = current.right;

				}
			}
		}
		return -1;
	}
	
	public int position(int val) {
		  return  positionHelper(val, root, 0);
		}

		public int positionHelper(int val, Node currentNode, int steps) {
		   // In-order search checks left node, then current node, then right node
		   if(currentNode.left != null) {
		       steps = positionHelper(val, currentNode.left, steps++);
		   }

		   // We found the node or have already moved over the node, return current steps
		   if(currentNode.value >= val) {
		       return steps;
		   }

		   // Next Node Index  
		   steps++;

		   if(currentNode.right != null) {
		       steps = positionHelper(val, currentNode.right, steps++);
		   }

		   return steps;
		}

	public String toString() {
		return toString(root, "", 0);
	}

	public String toString(Node current, String tabs, int level) {
		if (current == null) {
			return "";
		}
		String str = toString(current.right, tabs + "\t", level + 1);
		str += String.format("%s%s [Level:%d]\n", tabs, current, level + 1);
		str += toString(current.left, tabs + "\t", level + 1);
		return str;
	}
}