package javalp;

public class Tree {

	private Node root;
	public Tree() {
		// 20180120844
		Node node1 = new Node(2);
		Node node2 = new Node(0);
		Node node3 = new Node(1);
		Node node4 = new Node(8);
		Node node5 = new Node(0);
		Node node6 = new Node(1);
		Node node7 = new Node(2);
		Node node8 = new Node(0);
		Node node9 = new Node(8);
		Node node10 = new Node(4);
		Node node11 = new Node(4);
		
		root = node6;
		
		node6.setChilds(node4, node10);
		
		node4.setChilds(node2, node5);
		node10.setChilds(node8, node11);
		node8.setChilds(node7, node9);
		node11.setChilds(node1, node3);
	}
	
	public void inOrder() {
		inOrder(root);
	}

	private void inOrder(Node root) {
		if (root != null) {
			inOrder(root.left);
			System.out.print(root);
			inOrder(root.right);
		}
	}
}