package question2;

public class TreeV1 extends Tree {

	public boolean insert(int value) {
		if (root == null) {
			root = new Node(value);
			return true;
		} else {
			return insert(root, value);
		}
	}
	
	private boolean insert(Node root, int value) {
		Node current = root;
		Node parent = null;
	
		while (current != null) {
			parent = current;
			if (value < current.value) {
				current = current.left;
			} else {
				current = current.right;
			}
		}
		
		if (value < parent.value) {
			parent.left = new Node(value);
		} else  {
			parent.right = new Node(value);
		}
		return true;
	}
	
		public boolean equals(Node tree1) {
			return equals(root, tree1);
		}
		
		
		public boolean equals(Node tree, Node tree1) {
			if(tree1 == null && tree == null) {
				return true;
			}
			if(tree1 == null || tree == null) return false;
			if (tree.value != tree1.value) return false;
			if (equals(tree.left, tree1.left) == false) return false;
			if (equals(tree.right, tree1.right) == false) return false;
			return true;
		}
		
		
	
}
