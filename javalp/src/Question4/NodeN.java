package Question4;

import java.util.ArrayList;

public class NodeN {

	//public NodeN right, left;
	public int value;
	public ArrayList<NodeN> childs;
	//public NodeN[] childs;
		
	
	public NodeN(int value) {
		this.value = value;
		childs = new ArrayList<>();
	}
	
	public String toString() {
		return Integer.toString(value);
	}
}
